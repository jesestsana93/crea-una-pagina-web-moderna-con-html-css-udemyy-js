$(function () {
    $('.filter').click(function () {//los elementos que contengan la clase filter
        $(this).addClass('active').siblings().removeClass('active');//se agrega al elemento activo la clase y a sus hermanos se les quita
        let valor = $(this).attr('data-nombre');
        if (valor == 'todos') {//contenedor trabajos
            $('.cont-work').show('1000');
        } else {
            $('.cont-work').not('.' + valor).hide('1000');//sino fue la clase que dimos en el if, que esta no la esconda y las demas si
            $('.cont-work').filter('.' + valor).show('1000');
        }
    });
    //clases de las secciones
    let equipo = $('#equipo').offset().top,
        servicio = $('#servicio').offset().top,
        trabajo = $('#trabajo').offset().top,
        contacto = $('#contacto').offset().top;

    window.addEventListener('resize', function(){//cuando cambia de tamaño vuelve a declarar las variables
        let equipo = $('#equipo').offset().top,
        servicio = $('#servicio').offset().top,
        trabajo = $('#trabajo').offset().top,
        contacto = $('#contacto').offset().top;
    });

    $('#enlace-inicio').on('click', function(e){
        e.preventDefault();//para que no aparezca al final el #
        $('html, body').animate({
            scrollTop: 0
        },600);
    });

    $('#enlace-equipo').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: equipo -100 //al hacer scroll queda en la seccion, sin que el menu tape
        },600);
    });

    $('#enlace-servicio').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: servicio -100
        },600);
    });

    $('#enlace-trabajo').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: trabajo -100
        },600);
    });

    $('#enlace-contacto').on('click', function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: contacto -100
        },600);
    });
});